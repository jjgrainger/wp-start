# Wp Start v1.0.0

> Scaffold a new WordPress site quickly

## Requirements

* You have [node](http://nodejs.org) installed
* You have [grunt-cli](http://gruntjs.com/getting-started) installed
* You have [grunt-init](http://gruntjs.com/project-scaffolding) installed

## Installation

To innstall the scaffold into the `.grunt-init` folder in your home directory, run the following in your terminal.

```
$ git clone git@github.com:jjgrainger/wp-start.git ~/.grunt-init/wp-start
```

_(Windows users, see [the documentation](http://gruntjs.com/project-scaffolding#installing-templates) for the correct destination directory path)_

## Usage

Create a project folder, then initialize the scaffold with `grunt-init wp-start`

```
$ grunt-init wp-start
```

## Notes

* Licensed under the [MIT License](LICENSE.md)
* Maintained under the [Semantic Versioning Guide](http://semver.org)
